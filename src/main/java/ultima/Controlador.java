package ultima;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.Application;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.primefaces.json.JSONObject;
import org.primefaces.ultima.domain.Domicilio;
import org.primefaces.ultima.service.DomicilioService;

@Controller
public class Controlador {
	@Autowired
	ServletContext context;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String consulta() {
		System.out.println("from Controlador.java :D");
		return "index";
	}
	

	@RequestMapping(value = "/validar", method = RequestMethod.GET)
	public  String validar() {
		System.out.println("valida");
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(null);
		} catch (JsonProcessingException e) { // Aqui va el log e.printStackTrace();
		}

		byte[] datos = jsonInString.getBytes();
		String url = "https://services-dot-my-project-tfja.appspot.com/v1/renapo";
		URL obj = null;
		HttpURLConnection con = null;
		StringBuffer response = null;
		try {

			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			con.setRequestProperty("curp", "KIJU234565");
			// con.setRequestProperty("Content-Type", "application/json");
			// OutputStream stream = con.getOutputStream();
			// stream.write(datos);
			// stream.flush();
			// stream.close();
			System.out.println(con.getResponseCode());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println("Response from service" + response);
			in.close();

		} catch (Exception e) { // TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("sin problemas");

		

		ObjectMapper mapperf = new ObjectMapper();
		Pojo user = null;
		try {
			user = mapperf.readValue(response.toString(), Pojo.class);
			System.out.println(user.getData().getApellido1());
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		 Data bean = (Data) context.getAttribute("Data");
		 bean.setApellido1(user.getData().getApellido1());
		return "index";
	
	}
	
	@RequestMapping(value = "/buscarDomicilioRenapo", method = RequestMethod.GET)
	public  String buscarDomicilioRenapo() {
		System.out.println("buscarDomicilioRenapo");
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(null);
		} catch (JsonProcessingException e) { // Aqui va el log e.printStackTrace();
		}

		byte[] datos = jsonInString.getBytes();
		String url = "https://services-dot-my-project-tfja.appspot.com/v1/renapo";
		URL obj = null;
		HttpURLConnection con = null;
		StringBuffer response = null;
		try {

			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			con.setRequestProperty("curp", "KIJU234565");
			// con.setRequestProperty("Content-Type", "application/json");
			// OutputStream stream = con.getOutputStream();
			// stream.write(datos);
			// stream.flush();
			// stream.close();
			System.out.println(con.getResponseCode());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println("Response from service" + response);
			in.close();

		} catch (Exception e) { // TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("sin problemas");

		
		DataDomicilio bean = (DataDomicilio) context.getAttribute("DataDomicilio");
	    bean.setCB_ENTIDADFEDERATIVA("Ciudad de México");
		bean.setCB_PAIS("México");
		
		
		 //ObjectMapper mapperf = new ObjectMapper(); 
		 //user = mapperf.readValue(response.toString(), Pojo.class);
		 
		/*
		 * ObjectMapper mapperf = new ObjectMapper();
		Pojo user = null;
		try {
			user = mapperf.readValue(response.toString(), Pojo.class);
			System.out.println(user.getData().getApellido1());
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		 Data bean = (Data) context.getAttribute("Data");
		 bean.setApellido1(user.getData().getApellido1());
		 
		 * */
		return "redirect:/JSFDomicilioRegistrar.jsf";
	
	}
	
	@RequestMapping(value = "/regDomicilio", method = RequestMethod.GET)	
	public  String registrarDomicilio() {

		
		Pojo domicilio = null;
		
		//DataDomicilio bean = (DataDomicilio) context.getAttribute("DataDomicilio");
		//bean.setCB_ENTIDADFEDERATIVA("CDMX");
		//bean.setCB_PAIS("México");		 
		
		System.out.println("Inside url regDomicilio");
		
		return "redirect:/JSFDomicilioConsultar.jsf";
		
	}
	
	@RequestMapping(value = "/modDomicilio", method = RequestMethod.GET)	
	public  String modificarDomicilio() {

		
		
		//DataDomicilio bean = (DataDomicilio) context.getAttribute("DataDomicilio");
		//bean.setCB_ENTIDADFEDERATIVA("CDMX");
		//bean.setCB_PAIS("México");		 
		
		System.out.println("Inside url regDomicilio");
		
		return "redirect:/JSFDomicilioConsultar.jsf";
		
	}
	
	@RequestMapping(value = "/eliminarDomicilio", method = RequestMethod.GET)	
	public  String eliminarDomicilio() {

		
		
		//DataDomicilio bean = (DataDomicilio) context.getAttribute("DataDomicilio");
		//bean.setCB_ENTIDADFEDERATIVA("CDMX");
		//bean.setCB_PAIS("México");		 
		
		System.out.println("Inside url eliminarDomicilio");
		
		return "redirect:/JSFDomicilioConsultar.jsf";
		
	}
	
	@RequestMapping(value = "/guardarDomicilio", method = RequestMethod.GET)	
	public  String guardarDomicilio() {

		
		
		//DataDomicilio bean = (DataDomicilio) context.getAttribute("DataDomicilio");
		//bean.setCB_ENTIDADFEDERATIVA("CDMX");
		//bean.setCB_PAIS("México");		 
		
		System.out.println("Inside url regDomicilio");
		
		return "redirect:/JSFDomicilioBuscar.jsf";
		
	}
	
	
	private List<Domicilio> list;	
	private DomicilioService service;
	
	@RequestMapping(value = "/getDomicilio", method = RequestMethod.GET)
	@ResponseBody
	public  List<Domicilio> consultarDomicilio() {
				

		 List<Domicilio> list = new ArrayList<Domicilio>();
		 list.add(new Domicilio("12","Brasil","Domicilio Particular","Calle:22, Aldana CP0033" ));
	     list.add(new Domicilio("13","Dinamarca","Domicilio Notificaciones", "Calle:Hidalgo, Luis Cabrera CP2034"));
	     list.add(new Domicilio("14","Suecia", "Domicilio Particular","Calle:22, Aldana CP0033" ));
	     
		return list;
	}
	


}
