package ultima;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
@ManagedBean(name="DataDomicilio")
@ApplicationScoped

public class DataDomicilio implements Serializable {

	    public String id;
	    public String CB_PAIS;
	    public int cp;
	    public String CB_TIPODOMICILIO;
	    public String CB_ENTIDADFEDERATIVA;
	    public String CB_MUNICIPIO;
	    public String ST_COLONIA;
	    public String ST_CALLE;
	    public String ST_NUMEROEXTERIOR;
	    public String ST_NUMEROINTERIOR;
	    public String ST_DIRECCION;
	    
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getCB_PAIS() {
			return CB_PAIS;
		}
		public void setCB_PAIS(String cB_PAIS) {
			CB_PAIS = cB_PAIS;
		}
		public int getCp() {
			return cp;
		}
		public void setCp(int cp) {
			this.cp = cp;
		}
		public String getCB_TIPODOMICILIO() {
			return CB_TIPODOMICILIO;
		}
		public void setCB_TIPODOMICILIO(String cB_TIPODOMICILIO) {
			CB_TIPODOMICILIO = cB_TIPODOMICILIO;
		}
		public String getCB_ENTIDADFEDERATIVA() {
			return CB_ENTIDADFEDERATIVA;
		}
		public void setCB_ENTIDADFEDERATIVA(String cB_ENTIDADFEDERATIVA) {
			CB_ENTIDADFEDERATIVA = cB_ENTIDADFEDERATIVA;
		}
		public String getCB_MUNICIPIO() {
			return CB_MUNICIPIO;
		}
		public void setCB_MUNICIPIO(String cB_MUNICIPIO) {
			CB_MUNICIPIO = cB_MUNICIPIO;
		}
		public String getST_COLONIA() {
			return ST_COLONIA;
		}
		public void setST_COLONIA(String sT_COLONIA) {
			ST_COLONIA = sT_COLONIA;
		}
		public String getST_CALLE() {
			return ST_CALLE;
		}
		public void setST_CALLE(String sT_CALLE) {
			ST_CALLE = sT_CALLE;
		}
		public String getST_NUMEROEXTERIOR() {
			return ST_NUMEROEXTERIOR;
		}
		public void setST_NUMEROEXTERIOR(String sT_NUMEROEXTERIOR) {
			ST_NUMEROEXTERIOR = sT_NUMEROEXTERIOR;
		}
		public String getST_NUMEROINTERIOR() {
			return ST_NUMEROINTERIOR;
		}
		public void setST_NUMEROINTERIOR(String sT_NUMEROINTERIOR) {
			ST_NUMEROINTERIOR = sT_NUMEROINTERIOR;
		}
		public String getST_DIRECCION() {
			return ST_DIRECCION;
		}
		public void setST_DIRECCION(String sT_DIRECCION) {
			ST_DIRECCION = sT_DIRECCION;
		}
	
	    
	
}
