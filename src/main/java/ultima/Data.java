package ultima;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
@ManagedBean(name="Data")
@ApplicationScoped
public class Data implements Serializable {

	private String numEntidadReg;
	private String apellido2;
	private String libro;
	private String statusCurp;
	@JsonIgnore
	private String TipoError;
	private String cveEntidadNac;
	private String apellido1; 
	private String numActa;
	private String CRIP;
	private String cveEntidadEmisora;
	private String anioReg;
	private String cveMunicipioReg;
	@JsonIgnore
	private String CodigoError;
	private String CURP;
	private String nombres;
	private String nacionalidad;
	@JsonIgnore
	private String message;
	@JsonIgnore
	private String statusOper;
	private String fechNac;
	@JsonIgnore
	private String SessionID;
	private String sexo;
	private String docProbatorio;
	public String getNumEntidadReg() {
		return numEntidadReg;
	}
	public void setNumEntidadReg(String numEntidadReg) {
		this.numEntidadReg = numEntidadReg;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public String getLibro() {
		return libro;
	}
	public void setLibro(String libro) {
		this.libro = libro;
	}
	public String getStatusCurp() {
		return statusCurp;
	}
	public void setStatusCurp(String statusCurp) {
		this.statusCurp = statusCurp;
	}
	public String getTipoError() {
		return TipoError;
	}
	public void setTipoError(String tipoError) {
		TipoError = tipoError;
	}
	public String getCveEntidadNac() {
		return cveEntidadNac;
	}
	public void setCveEntidadNac(String cveEntidadNac) {
		this.cveEntidadNac = cveEntidadNac;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getNumActa() {
		return numActa;
	}
	public void setNumActa(String numActa) {
		this.numActa = numActa;
	}
	public String getCRIP() {
		return CRIP;
	}
	public void setCRIP(String cRIP) {
		CRIP = cRIP;
	}
	public String getCveEntidadEmisora() {
		return cveEntidadEmisora;
	}
	public void setCveEntidadEmisora(String cveEntidadEmisora) {
		this.cveEntidadEmisora = cveEntidadEmisora;
	}
	public String getAnioReg() {
		return anioReg;
	}
	public void setAnioReg(String anioReg) {
		this.anioReg = anioReg;
	}
	public String getCveMunicipioReg() {
		return cveMunicipioReg;
	}
	public void setCveMunicipioReg(String cveMunicipioReg) {
		this.cveMunicipioReg = cveMunicipioReg;
	}
	public String getCodigoError() {
		return CodigoError;
	}
	public void setCodigoError(String codigoError) {
		CodigoError = codigoError;
	}
	public String getCURP() {
		return CURP;
	}
	public void setCURP(String cURP) {
		CURP = cURP;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatusOper() {
		return statusOper;
	}
	public void setStatusOper(String statusOper) {
		this.statusOper = statusOper;
	}
	public String getFechNac() {
		return fechNac;
	}
	public void setFechNac(String fechNac) {
		this.fechNac = fechNac;
	}
	public String getSessionID() {
		return SessionID;
	}
	public void setSessionID(String sessionID) {
		SessionID = sessionID;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getDocProbatorio() {
		return docProbatorio;
	}
	public void setDocProbatorio(String docProbatorio) {
		this.docProbatorio = docProbatorio;
	}
	
	
}
