package ultima;

import java.io.Serializable;


public class Pojo implements Serializable {
	private Error error;
	private Data data;
	private DataDomicilio dataDomicilio;
	
	
	public Error getError() {
		return error;
	}
	
	public void setError(Error error) {
		this.error = error;
	}
	
	public Data getData() {
		return data;
	}
	
	public void setData(Data data) {
		this.data = data;
	}
	
	
	public DataDomicilio getDataDomicilio() {
		return dataDomicilio;
	}
	
	public void setDataDomicilio(DataDomicilio dataDomicilio) {
		this.dataDomicilio = dataDomicilio;
	}

}
