package ultima;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Error implements Serializable {

private String Message;
private String Error;
public Error() {
	
	
}

public String getMessage() {
	return Message;
}
public void setMessage(String message) {
	Message = message;
}
public String getError() {
	return Error;
}
public void setError(String error) {
	Error = error;
}


}
