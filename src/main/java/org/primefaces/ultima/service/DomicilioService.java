/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.primefaces.ultima.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;


import org.primefaces.ultima.domain.Domicilio;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ultima.Data;
import ultima.Pojo;

@ManagedBean(name = "domicilioService")
@ApplicationScoped
public class DomicilioService {
    
	/*
	 * 				 String id, 
    		         String CB_PAIS, 
    		         int cp, 
    		         String CB_TIPODOMICILIO, 
    		         String CB_ENTIDADFEDERATIVA, 
    		         String CB_MUNICIPIO, 
    		         String ST_COLONIA, 
    		         String ST_CALLE,
    		         String ST_NUMEROEXTERIOR,
    		         String ST_NUMEROINTERIOR,
    		         String ST_DIRECCION
	 * */
	@Autowired
	ServletContext context;
    
    public void registrarDomicilio() {
		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			System.out.print("inside conectar function");
			context.redirect(context.getRequestContextPath() + "regDomicilio");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void modificarDomicilio() {			
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			System.out.print("inside modificarDomicilio");
			context.redirect(context.getRequestContextPath() + "modDomicilio");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void guardarDomicilio() {			
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			System.out.print("inside guardarDomicilio");
			context.redirect(context.getRequestContextPath() + "guardarDomicilio");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void eliminarDomicilio() {
		System.out.print("inside eliminarDomicilio");
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "eliminarDomicilio");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	public void buscarDomicilioRenapo() {
		System.out.print("inside eliminarDomicilio");
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "buscarDomicilioRenapo");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
		
	private Boolean renderInputText = true;
	public void setRenderInputText(Boolean renderInputText) {
	    this.renderInputText = renderInputText;
	}

	public Boolean getRenderInputText() {
	   return renderInputText;
	}
	
	//obtener el valor del componente select 
	public void valueChanged(ValueChangeEvent event) {
	    //do your stuff
		
		System.out.println("### ValueChangeEvent");
		System.out.println(event.getNewValue());
		
		if(event.getNewValue().equals("Extranjero")) {
			
			setRenderInputText(false);
		}else {
			setRenderInputText(true);
			
		}
			
		
	}
	
	
	
    public  List<Domicilio> createDomicilioExtrangeroURLJson() {
		System.out.println("inside service to get domicilios...");
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(null);
		} catch (JsonProcessingException e) { // Aqui va el log e.printStackTrace();
		}

		byte[] datos = jsonInString.getBytes();
		String url = "http://localhost:8080/getDomicilio";
		URL obj = null;
		HttpURLConnection con = null;
		StringBuffer response = null;
		
		try {

			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("GET");

			System.out.println(con.getResponseCode());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String inputLine;
			response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println("Response from service" + response);
			in.close();

		} catch (Exception e) { // TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("sin problemas");

		
		ObjectMapper mapperf = new ObjectMapper();

		List<Domicilio> list = new ArrayList<Domicilio>();
		
		try {
			//String a lista de objetos
			List<Domicilio> domicilios = mapperf.readValue(response.toString(), new TypeReference<ArrayList<Domicilio>>(){});

			int size=domicilios.size();
						
			for(int x=0;x<domicilios.size();x++) {
				  	
				  System.out.println(domicilios.get(x).id);
				  System.out.println(domicilios.get(x).CB_PAIS);
				  System.out.println(domicilios.get(x).CB_TIPODOMICILIO);
				  System.out.println(domicilios.get(x).ST_DIRECCION);
				  
				  list.add(new Domicilio(domicilios.get(x).id,domicilios.get(x).CB_PAIS,domicilios.get(x).CB_TIPODOMICILIO,domicilios.get(x).ST_DIRECCION ));
				  list.add(new Domicilio(domicilios.get(x).id,domicilios.get(x).CB_PAIS,domicilios.get(x).CB_TIPODOMICILIO,domicilios.get(x).ST_DIRECCION ));
				  
			}
		//
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	  
	        
	        
		return list;
	
	}
    
    public  String registrarDomicilioExtrangero() {
    	System.out.println("valida");
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(null);
		} catch (JsonProcessingException e) { // Aqui va el log e.printStackTrace();
		}

		byte[] datos = jsonInString.getBytes();
		String url = "https://services-dot-my-project-tfja.appspot.com/v1/renapo";
		URL obj = null;
		HttpURLConnection con = null;
		StringBuffer response = null;
		try {

			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			con.setRequestProperty("curp", "KIJU234565");
			// con.setRequestProperty("Content-Type", "application/json");
			// OutputStream stream = con.getOutputStream();
			// stream.write(datos);
			// stream.flush();
			// stream.close();
			System.out.println(con.getResponseCode());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println("Response from service" + response);
			in.close();

		} catch (Exception e) { // TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("sin problemas");

		

		ObjectMapper mapperf = new ObjectMapper();
		Pojo user = null;
		try {
			user = mapperf.readValue(response.toString(), Pojo.class);
			System.out.println(user.getData().getApellido1());
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		 Data bean = (Data) context.getAttribute("Data");
		 bean.setApellido1(user.getData().getApellido1());
		return "index";
	        
		
	
	}

}
