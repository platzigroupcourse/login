/**
     * Faces Validator
     */
     
     $(document).ready(function() {
   
    	 $.validator.addMethod("regex", function(value, element, regexpr) {          
    	     return regexpr.test(value);
    	   }, "Please enter a valid pasword.");    
    	 
    	 
    	 $("#form").validate({    		 
    		 
    			rules: {
    				"form:email": {
    				     
    				      //regex: /[A-Za-z0-9\w]{4,20}/,
    				    	  required: true
    				    },
    				 "form:nombre": {
       				     
      				      //regex: /[A-Za-z0-9\w]{4,20}/,
      				    	  required: true
      				    }
    	 
    			},
    	 
    	    messages: {
    	    	"form:email": {
    	    		
    	    		required: "Please provide email",
    	    	//	regex: "invalido"
    	    		
    	    	},
    	    	
    	    	"form:nombre": {
    	    		
    	    		required: "Please provide name",
    	    	//	regex: "invalido"
    	    		
    	    	}
    	        
    	        }
    		});    	 
    	 
     });
   
    
    